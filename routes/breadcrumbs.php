<?php

use App\Models\Map;
use App\Models\Task;
use App\Models\User;
use App\Models\Weather;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

/**
 * Admin
 */
Breadcrumbs::for('dashboard', function(BreadcrumbTrail $breadcrumbs) {
    $breadcrumbs->push('Админ Панель', route('dashboard'));
});


/**
 * Admin / Users
 */
Breadcrumbs::for('user.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Сотрудники', route('user.index'));
});

/**
 * Admin / Users / Create User
 */
Breadcrumbs::for('user.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push('Добавить Сотрудника', route('user.create'));
});

/**
 * Admin / Users / [User Name]
 */
Breadcrumbs::for('user.show', function(BreadcrumbTrail $breadcrumbs, user $user)
{
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push($user->first_name, route('user.show', $user->id));
});

/**
 * Admin / Users / [User Name] / Edit User
 */
Breadcrumbs::for('user.edit', function(BreadcrumbTrail $breadcrumbs, user $user)
{
    $breadcrumbs->parent('user.show', $user);
    $breadcrumbs->push('Редактировать Сотрудника', route('user.edit', $user->id));
});


/**
 * Admin / Maps
 */
Breadcrumbs::for('map.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Операции', route('map.index'));
});

/**
 * Admin / Maps / Create Map
 */
Breadcrumbs::for('map.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('map.index');
    $breadcrumbs->push('Добавить Операцию', route('map.create'));
});

/**
 * Admin / Maps / [Map Name]
 */
Breadcrumbs::for('map.show', function(BreadcrumbTrail $breadcrumbs, Map $map)
{
    $breadcrumbs->parent('map.index');
    $breadcrumbs->push($map->name, route('map.show', $map->id));
});

/**
 * Admin / Maps / [Map Name] / Edit Map
 */
Breadcrumbs::for('map.edit', function(BreadcrumbTrail $breadcrumbs, Map $map)
{
    $breadcrumbs->parent('map.show', $map);
    $breadcrumbs->push('Редактировать Операцию', route('map.edit', $map->id));
});


/**
 * Admin / Tasks
 */
Breadcrumbs::for('task.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Задачи', route('task.index'));
});

/**
 * Admin / Tasks / Create Task
 */
Breadcrumbs::for('task.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('task.index');
    $breadcrumbs->push('Добавить Задачу', route('task.create'));
});

/**
 * Admin / Tasks / [Task Name]
 */
Breadcrumbs::for('task.show', function(BreadcrumbTrail $breadcrumbs, Task $task)
{
    $breadcrumbs->parent('task.index');
    $breadcrumbs->push($task->map->name, route('task.show', $task->id));
});

/**
 * Admin / Tasks / [Task Name] / Edit Task
 */
Breadcrumbs::for('task.edit', function(BreadcrumbTrail $breadcrumbs, Task $task)
{
    $breadcrumbs->parent('task.show', $task);
    $breadcrumbs->push('Редактировать Задачу', route('task.edit', $task->id));
});


/**
 * Admin / Weathers
 */
Breadcrumbs::for('weather.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Показатели', route('weather.index'));
});

/**
 * Admin / Weathers / Create Weather
 */
Breadcrumbs::for('weather.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('weather.index');
    $breadcrumbs->push('Добавить Показатель', route('weather.create'));
});

/**
 * Admin / Weathers / [Weather Name]
 */
Breadcrumbs::for('weather.show', function(BreadcrumbTrail $breadcrumbs, Weather $weather)
{
    $breadcrumbs->parent('weather.index');
    $breadcrumbs->push($weather->id, route('weather.show', $weather->id));
});

/**
 * Admin / Weathers / [Weather Name] / Edit Weather
 */
Breadcrumbs::for('weather.edit', function(BreadcrumbTrail $breadcrumbs, Weather $weather)
{
    $breadcrumbs->parent('weather.show', $weather);
    $breadcrumbs->push('Редактировать Показатель', route('weather.edit', $weather->id));
});


/**
 * Admin / Weathers
 */
Breadcrumbs::for('analytics.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Аналитика', route('analytics.index'));
});
