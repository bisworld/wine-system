<?php

use App\Http\Controllers\AnalyticsController;
use App\Http\Controllers\MapController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WeatherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::resource('user', UserController::class);

    Route::resource('map', MapController::class);

    Route::resource('task', TaskController::class);

    Route::resource('weather', WeatherController::class);

    Route::resource('analytics', AnalyticsController::class);
});

require __DIR__.'/auth.php';
