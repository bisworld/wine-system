/**
 * Created by starcode on 19.01.15.
 */
(function( $ )
{
    // Plugin
    $.fn.select = function( options )
    {
        return this.each( function()
        {
            $.data( this, 'select', {} );
            $.data( this, 'select', Select( this, options ));
        });
    };

    // Initialization
    function Select( el, options )
    {
        return new Select.prototype.init( el, options );
    }

    $.Select         = Select;
    $.Select.NAME    = 'select';
    $.Select.VERSION = '1.0';
    $.Select.opts    = {

        /**
         * Settings
         */
        placeholder: 'Выберите значение...',
    };

    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function( elem ) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    /**
     * Functionality
     */
    Select.fn = $.Select.prototype = {

        /**
         * Initialization
         *
         * @param el
         * @param options
         */
        init: function( el, options )
        {
            this.$element = ( el !== false )
                ? $( el )
                : false;

            this.loadOptions( options );
            this.defaultValues();
            this.build();
        },

        /**
         * Загрузка опций
         *
         * @param options
         */
        loadOptions: function( options )
        {
            this.opts = $.extend(
                {},
                $.extend( true, {}, $.Select.opts ),
                this.$element.data(),
                options
            );
        },

        /**
         * Значения по умолчанию
         */
        defaultValues: function()
        {
            this.$disabled = this.$element.prop('disabled');
            this.$multiple = this.$element.prop('multiple');
            this.$open     = false;
        },

        /**
         * Создание элемента и его событий
         */
        build: function()
        {
            this.$element.wrap('<div class="select-wrap" />');
            this.$wrapper = this.$element.parent('.select-wrap');

            /**
             * Если select multiple
             */
            ( this.$multiple )
                ? this.buildMultiple()
                : this.buildSingle();

            /**
             * Если select disabled
             */
            if ( this.$disabled )
                return;

            /**
             * Обработчик клика на элементе
             */
            this.$wrapper.on('click', $.proxy( this.toggleList, this ));

            /**
             * Обработчик клика на документе
             */
            $(document).on('click', $.proxy( this.close, this ));
        },

        /**
         * Single select
         */
        buildSingle: function()
        {
            this.$selected = $('<div class="selected" />');
            this.$wrapper.append( this.$selected );

            let value = null;
            let self  = this;
            let empty = $('<option />', {
                text: null,
                value: null,
                hidden: true,
                disabled: true
            });

            this.$element.prepend( empty );

            this.$element.children('option').each(function () {
                if ( $(this).attr('selected') ) {
                    value = $(this).text();
                }
            });

            if (! value) {
                empty.attr('selected', true);
            }

            this.input = $('<input />', {
                type: 'text',
                value: value,
                placeholder: this.opts.placeholder
            }).on('keyup', function( event ) {
                if ( event.which === 8 || event.which === 46 ) {
                    $(this).val(null);
                    empty.prop('selected', true );
                    self.$element.prop('selectedIndex', 0).trigger('change');
                    self.$options.children('li:not(.hidden)').removeClass('disabled');
                }

                self.search( $(this).val() );
            });

            this.$selected.append( this.input );
        },

        /**
         * Multiple select
         */
        buildMultiple: function()
        {
            this.$multi = $('<ul class="multi">');
            this.$input = $('<li><input type="text" placeholder="' + this.opts.placeholder + '"></li>');

            this.$multi.append( this.$input );
            this.$wrapper.append( this.$multi );

            if ( this.$disabled )
                this.$input.children().prop('disabled', true);

            let items = $('option:selected', this.$element);

            $.map( items, $.proxy( this.buildMultiItem, this ));
        },

        /**
         * Создание выбранного пункта в multiple
         *
         * @param item
         */
        buildMultiItem: function( item )
        {
            let option = $('<li />', {
                text: $(item).text(),
                rel: $(item).val()
            });

            if ( ! this.$disabled )
                $('<a />', {
                    html: '&times',
                    href: 'javascript:void(0)',
                    'class': 'remove'
                }).appendTo( option )
                    .on('click', $.proxy( this.removeItem, this ));


            this.$input.before( option );
        },

        /**
         * Удаление выбранного пункта в multiple
         *
         * @param e
         */
        removeItem: function( e )
        {
            e.stopPropagation();

            let item = $(e.currentTarget).parent('li');

            $('option[value="' + item.attr('rel') + '"]', this.$element )
                .prop('selected', false )
                .trigger('change');

            item.remove();

            $('li[rel="' + item.attr('rel') + '"]', this.$options )
                .removeClass('disabled');
        },

        /**
         * Открыть / закрыть select
         * @param e
         */
        toggleList: function( e )
        {
            e.stopPropagation();

            ( this.$open )
                ? this.close()
                : this.open();
        },

        /**
         * Открыть select
         */
        open: function()
        {
            $(document).trigger('click');

            if ( this.$selected )
                this.$selected.addClass('active');

            this.buildList();
            this.$open = true;
        },

        /**
         * Закрыть select
         */
        close: function()
        {
            if ( this.$selected )
                this.$selected.removeClass('active');

            if ( this.$options )
                this.$options.remove();

            this.$open = false;
        },

        /**
         * Создание выпадающего списка
         */
        buildList: function ()
        {
            this.$options = $('<ul class="options" />');
            this.$wrapper.append(this.$options);
            this.$list    = $('option', this.$element );

            $.map( this.$list, $.proxy( this.buildItem, this ));

            if ($(window).height() - this.$wrapper.outerHeight() - (this.$wrapper.offset().top - $(window).scrollTop()) > (this.$options.outerHeight() + 20)) {
                // раскрытие вниз
                this.$options.removeClass('up');
            } else {
                // раскрытие вверх
                this.$options.addClass('up');
            }
        },

        /**
         * Создание элемента списка
         *
         * @param item
         */
        buildItem: function( item )
        {
            let li = $('<li />', {
                text: $(item).text(),
                rel: $(item).val()
            }).appendTo(this.$options)
                .on('click', $.proxy(this.selected, this));

            if ( $(item).prop('selected') || $(item).prop('disabled'))
                li.addClass('disabled');

            if ( $(item).prop('hidden'))
                li.addClass('hidden');
        },

        /**
         * Клик по элементу списка
         *
         * @param e
         */
        selected: function( e )
        {
            let item = $(e.currentTarget);

            if ( item.hasClass('disabled') )
                return;

            let selected = $('option[value="' + item.attr('rel') + '"]', this.$element);

            selected.prop('selected', true ).trigger('change');

            ( this.$multiple )
                ? this.buildMultiItem( selected )
                : this.$selected.children('input').val( selected.text() );
        },

        /**
         * Поиск
         *
         * @param value
         */
        search: function ( value ) {
            this.$options.children('li:not(:contains("' + value + '"))').addClass('hidden');
            this.$options.children('li:not(.disabled.hidden):contains("' + value + '")')
                .removeClass('hidden');
        }
    };

    // constructor
    Select.prototype.init.prototype = Select.prototype;
})(jQuery);
