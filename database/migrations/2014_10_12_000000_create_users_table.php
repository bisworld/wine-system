<?php

use App\Dictionary\UserRole;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->enum('role', array_keys(UserRole::ROLES));
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name');
            $table->string('phone')->nullable()->unique();
            $table->string('password');
            $table->rememberToken();
            $table->string('telegram')->nullable()->unique();
            $table->smallInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
