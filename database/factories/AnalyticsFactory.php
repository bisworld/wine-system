<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Analytics>
 */
class AnalyticsFactory extends Factory
{
    const VINE = [
        'Изабелла',
        'Киш-миш',
        'Дамский пальчик',
        'Каберне Совиньон',
        'Шардоне',
    ];

    const PROB   = [
        'Высокая',
        'Средняя',
        'Подтвержденная',
    ];

    const HUMI   = [
        'Благоприятствует',
        'Не Благоприятствует',
    ];

    const ANAL   = [
        'Подтверждает',
        'Не Подтверждает',
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => self::VINE[fake()->numberBetween(0, 4)] ?? self::VINE[0],
            'probability' => self::PROB[fake()->numberBetween(0, 2)] ?? self::PROB[0],
            'humidity' => self::HUMI[fake()->numberBetween(0, 1)] ?? self::HUMI[0],
            'precipitation' => self::HUMI[fake()->numberBetween(0, 1)] ?? self::HUMI[0],
            'temperature' => self::HUMI[fake()->numberBetween(0, 1)] ?? self::HUMI[0],
            'period' => 'Текущий',
            'analysis' => self::ANAL[fake()->numberBetween(0, 1)] ?? self::ANAL[0],
            'duration' => 'Не достаточна',
        ];
    }
}
