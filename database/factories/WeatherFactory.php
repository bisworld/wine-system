<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\weather>
 */
class WeatherFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'temperature' => fake()->numberBetween(15, 35),
            'humidity' => fake()->numberBetween(25, 100),
            'pressure' => fake()->numberBetween(600, 800),
            'precipitation' => fake()->numberBetween(10, 80),
            'wind' => fake()->numberBetween(5, 25),
        ];
    }
}
