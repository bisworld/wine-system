<?php

namespace Database\Factories;

use App\Models\Map;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'map_id' => function () {
                return Map::query()->inRandomOrder()->first()->id;
            },
            'user_id' => function () {
                return User::query()->inRandomOrder()->first()->id;
            },
            'comment' => fake()->text(100),
            'status' => fake()->numberBetween(0, 1),
        ];
    }
}
