<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Map>
 */
class MapFactory extends Factory
{
    const NAME = [
        'Прополка',
        'Полив',
        'Сбор',
        'Обрезка',
        'Подвязка',
        'Обломка',
    ];

    const UNIT = [
        'га',
        'шт',
        'куст',
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name'  => self::NAME[fake()->numberBetween(0, 5)] ?? self::NAME[0],
            'unit'  => self::UNIT[fake()->numberBetween(0, 2)] ?? self::UNIT[0],
            'demand'  => 'нет',
            'volume'  => fake()->numberBetween(50, 100),
            'term'  => fake()->date('d.m.Y H:i:s'),
            'norm'  => fake()->numberBetween(50, 100),
            'days'  => fake()->numberBetween(1, 20),
        ];
    }
}
