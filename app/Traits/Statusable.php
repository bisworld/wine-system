<?php

namespace App\Traits;

use Html;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;

/**
 * Class Statusable
 *
 * @property array $default
 * @property array $statusable
 * @package App\Traits
 */
trait Statusable
{
    /**
     * Значение статуса по умолчанию
     *
     * @var array
     */
    protected $default = [
        'name' => 'Неизвестно',
        'icon' => 'fa fa-ban'
    ];

    /**
     * The attributes that are status list.
     *
     * @var array
     */
    protected $statusable = [
        0 => [
            'name' => 'Не активен',
            'icon' => 'fa fa-lock'
        ],
        1 => [
            'name' => 'Активен',
            'icon' => 'fa fa-check'
        ],
    ];

    /**
     * Статус в виде иконки
     *
     * @return HtmlString
     */
    public function getStatusIconAttribute(): HtmlString
    {
        $status = collect($this->statusable)->get($this->status, $this->default);
        $icon   = Html::tag('i', '', ['class' => $status['icon']])->toHtml();

        return Html::tag('span', $icon, ['class' => 'status icon', 'data-tooltip' => $status['name']]);
    }

    /**
     * Статус в виде текста
     *
     * @return string
     */
    public function getStatusTextAttribute(): string
    {
        return collect($this->statusable)->get($this->status, $this->default)['name'];
    }

    /**
     * @return HtmlString
     */
    public function getStatusLabelAttribute(): HtmlString
    {
        $status = collect($this->statusable)->get($this->status, $this->default);

        return Html::tag('span', $status['name'], ['class' => 'label ' . $status['class']]);
    }

    /**
     * Статусы в виде списка
     *
     * @return Collection
     */
    public function statusList(): Collection
    {
        return collect($this->statusable)->pluck('name');
    }
}
