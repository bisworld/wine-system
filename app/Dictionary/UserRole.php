<?php

namespace App\Dictionary;

interface UserRole
{
    public const ROLE_OWNER = 'owner';
    public const ROLE_MAIN = 'main';
    public const ROLE_AGRO = 'agro';
    public const ROLE_CHIEF = 'chief';
    public const ROLE_WINE = 'wine';

    public const ROLES = [
        self::ROLE_OWNER => 'Энолог',
        self::ROLE_MAIN => 'Главный аграном',
        self::ROLE_AGRO => 'Агроном',
        self::ROLE_CHIEF => 'Начальник бригады',
        self::ROLE_WINE => 'Виноградарь',
    ];
}
