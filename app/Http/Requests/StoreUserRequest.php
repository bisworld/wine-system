<?php

namespace App\Http\Requests;

use App\Dictionary\UserRole;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'role'        => ['required', 'string', Rule::in(array_keys(UserRole::ROLES))],
            'first_name'  => ['required', 'string'],
            'last_name'   => ['required', 'string'],
            'middle_name' => ['required', 'string'],
            'phone'       => ['nullable', 'string', 'unique:users'],
            'telegram'    => ['nullable', 'string', 'unique:users'],
            'status'      => ['required', 'string'],
            'password'    => ['required', 'string'],
        ];
    }
}
