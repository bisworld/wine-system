<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMapRequest;
use App\Http\Requests\UpdateMapRequest;
use App\Models\Map;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('map.index', [
            'maps' => Map::query()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('map.create', ['map' => new Map()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMapRequest $request
     * @return RedirectResponse
     */
    public function store(StoreMapRequest $request)
    {
        Map::query()->create($request->validated());

        return redirect()->route('map.index')->with('success', 'Операция Добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param Map $map
     * @return View
     */
    public function show(Map $map)
    {
        return view('map.show', ['map' => $map]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Map $map
     * @return View
     */
    public function edit(Map $map)
    {
        return view('map.edit', ['map' => $map]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMapRequest $request
     * @param Map              $map
     * @return RedirectResponse
     */
    public function update(UpdateMapRequest $request, Map $map)
    {
        $map->update($request->validated());

        return redirect()->route('map.index')->with('success', 'Операции Обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Map $map
     * @return JsonResponse
     */
    public function destroy(Map $map)
    {
        $map->delete();

        return response()->json(['status' => 'success', 'message' => 'Операции Удалена!']);
    }
}
