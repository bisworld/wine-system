<?php

namespace App\Http\Controllers;

use App\Models\Analytics;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class AnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('analytics.index', [
            'analytics' => Analytics::query()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return RedirectResponse
     */
    public function create()
    {
        Analytics::factory(10)->create();

        return redirect()->route('analytics.index')->with('success', 'Аналитика Произведена!');
    }
}
