<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreWeatherRequest;
use App\Http\Requests\UpdateWeatherRequest;
use App\Models\Weather;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class WeatherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        Weather::factory()->create();

        return view('weather.index', [
            'weathers' => Weather::query()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('weather.create', ['weather' => new Weather()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreWeatherRequest $request
     * @return RedirectResponse
     */
    public function store(StoreWeatherRequest $request)
    {
        Weather::query()->create($request->validated());

        return redirect()->route('weather.index')->with('success', 'Показатель Добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param Weather $weather
     * @return View
     */
    public function show(Weather $weather)
    {
        return view('weather.show', ['weather' => $weather]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Weather $weather
     * @return View
     */
    public function edit(Weather $weather)
    {
        return view('weather.edit', ['weather' => $weather]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateWeatherRequest $request
     * @param Weather              $weather
     * @return RedirectResponse
     */
    public function update(UpdateWeatherRequest $request, Weather $weather)
    {
        $weather->update($request->validated());

        return redirect()->route('weather.index')->with('success', 'Показатель Обновлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Weather $weather
     * @return JsonResponse
     */
    public function destroy(Weather $weather)
    {
        $weather->delete();

        return response()->json(['status' => 'success', 'message' => 'Показатель Удален!']);
    }
}
