<?php

namespace App\Http\Middleware;

use Menu;
use Closure;
use Lavary\Menu\Builder;
use Illuminate\Http\Request;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        Menu::make('adminMenu', function ($menu) {
            /** @var $menu Builder */
            $menu->add('Админ Панель', ['route' => 'dashboard'])
                 ->prepend('<i class="fa fa-dashboard"></i> ');

            $menu->add('Сотрудники', ['route' => 'user.index'])
                 ->prepend('<i class="fa fa-users"></i> ')
                 ->active('users/*');

            $menu->add('Тех. Карта', ['route' => 'map.index'])
                ->prepend('<i class="fa fa-file"></i> ')
                ->active('map/*');

            $menu->add('Задачи', ['route' => 'task.index'])
                ->prepend('<i class="fa fa-list"></i> ')
                ->active('task/*');

            $menu->add('Метеостанция', ['route' => 'weather.index'])
                ->prepend('<i class="fa fa-sun-o"></i> ')
                ->active('weather/*');

            $menu->add('Аналитика', ['route' => 'analytics.index'])
                ->prepend('<i class="fa  fa-bar-chart"></i> ')
                ->active('analytics/*');

            $menu->add('Настройки', [])
                ->prepend('<i class="fa fa-cogs"></i> ');
        });

        return $next($request);
    }
}
