@section('title', 'Добавление Показателя')
@section('section', 'Управление Показателями')
@section('breadcrumbs', Breadcrumbs::render('weather.create'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Показателя</h2>
        </header>

        {!! Form::open(['route' => 'weather.store', 'class' => 'form body-tile']) !!}
        @include('weather.form', ['weather' => $weather])
        {!! Form::close() !!}
    </section>
</x-app-layout>
