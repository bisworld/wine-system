@section('title', 'Обзор Аналитики')
@section('section', 'Управление Аналитикой')
@section('breadcrumbs', Breadcrumbs::render('analytics.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Аналитики</h2>

            <nav class="controls">
                <a href="{!! route('analytics.create') !!}"><i class="fa fa-plus"></i> Рекомендации</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Вероятность</th>
                <th>Влажность</th>
                <th>Осадки</th>
                <th>Температура</th>
                <th>Период</th>
                <th>Анализ</th>
                <th>Длительность</th>
            </tr>
            </thead>
            <tbody>
            @forelse($analytics as $analytic)
                <tr>
                    <td>{{ $analytic->id }}.</td>
                    <td>{{ $analytic->name }}</td>
                    <td><a href="#">{{ $analytic->probability }}</a></td>
                    <td>{{ $analytic->humidity }}</td>
                    <td>{{ $analytic->precipitation }}</td>
                    <td>{{ $analytic->temperature }}</td>
                    <td>{{ $analytic->period }}</td>
                    <td><a href="#">{{ $analytic->analysis }}</a></td>
                    <td>{{ $analytic->duration }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="9">
                        <div class="message warning" role="alert">Аналитика не найдена!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $analytics->links() }}
    </section>
</x-app-layout>
