@section('title', 'Просмотр Операции')
@section('section', 'Управление Операциями')
@section('breadcrumbs', Breadcrumbs::render('map.show', $map))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Операции</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Имя:</span>
                    <span class="inp">{{ $map->first_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Фамилия:</span>
                    <span class="inp">{{ $map->last_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Отчество:</span>
                    <span class="inp">{{ $map->middle_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Telegram:</span>
                    <span class="inp">{{ $map->telegram }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Телефон:</span>
                    <span class="inp">{{ $map->phone }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Роль:</span>
                    <span class="inp">{{ $map->role_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Статус:</span>
                    <span class="inp">{{ $map->status_text }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Дата Создания:</span>
                    <span class="inp">{{ $map->created_at }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Дата Изменения:</span>
                    <span class="inp">{{ $map->updated_at }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
</x-app-layout>
