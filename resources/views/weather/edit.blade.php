@section('title', 'Редактирование Показателя')
@section('section', 'Управление Показателями')
@section('breadcrumbs', Breadcrumbs::render('weather.edit', $weather))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Показателя</h2>
        </header>

        {!! Form::model($weather, ['route' => ['weather.update', $weather->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('weather.form', ['weather' => $weather])
        {!! Form::close() !!}
    </section>
</x-app-layout>
