@section('title', 'Обзор Показателей')
@section('section', 'Управление Показателями')
@section('breadcrumbs', Breadcrumbs::render('weather.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Показателей</h2>

            <nav class="controls">
                <a href="{!! route('weather.create') !!}"><i class="fa fa-plus"></i> Добавить Показатель</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Температура</th>
                <th>Влажность</th>
                <th>Давление</th>
                <th>Осадки</th>
                <th>Ветер</th>
                <th>Дата Добавления</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($weathers as $weather)
                <tr>
                    <td>{{ $weather->id }}.</td>
                    <td>{{ $weather->temperature }}</td>
                    <td>{{ $weather->humidity }}</td>
                    <td>{{ $weather->pressure }}</td>
                    <td>{{ $weather->precipitation }}</td>
                    <td>{{ $weather->wind }}</td>
                    <td>@datetime($weather->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('weather.show', [$weather->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('weather.edit', [$weather->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $weather->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="8">
                        <div class="message warning" role="alert">Показатели не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $weathers->links() }}
    </section>
</x-app-layout>
