<div class="row">
    <div class="col form-item">
        {!! Form::label('temperature', 'Температура:') !!}
        {!! Form::text('temperature', null, ['class' => 'inp', 'placeholder' => 'Наименование', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('humidity', 'Влажность:') !!}
        {!! Form::text('humidity', null, ['class' => 'inp', 'placeholder' => 'Куст', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('pressure', 'Давление:') !!}
        {!! Form::text('pressure', null, ['class' => 'inp', 'placeholder' => 'Требования']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('precipitation', 'Осадки:') !!}
        {!! Form::text('precipitation', null, ['class' => 'inp', 'placeholder' => 'Объем', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('wind', 'Ветер:') !!}
        {!! Form::text('wind', null, ['class' => 'inp', 'placeholder' => 'Срок', 'required']) !!}
    </div>
</div>

<footer class="row footer-tile">
    <a href="{{ URL::previous() }}" class="button light-red">Отмена</a>
    <input type="submit" value="Сохранить" class="button success">
</footer>
