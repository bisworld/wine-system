@section('title', 'Главная Страница')
@section('section', 'Админ Панель')
@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Главная</strong> Страница</h2>
        </header>

        <article class="body-tile">
            <p>Здесь будет общая информация о системе и статистика</p>
        </article>
    </section>
</x-app-layout>
