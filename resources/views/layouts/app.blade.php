<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>@yield('title')</title>
    {!! Html::favicon('favicon.ico') !!}
    {!! Html::style('css/reset.css') !!}
    {!! Html::style('css/grid.css') !!}
    {!! Html::style('css/table.css') !!}
    {!! Html::style('css/form.css') !!}
    {!! Html::style('css/drop-down.css') !!}
    {!! Html::style('css/bread-crumb.css') !!}
    {!! Html::style('css/pagination.css') !!}
    {!! Html::style('css/message.css') !!}
    {!! Html::style('css/tooltip.css') !!}
    {!! Html::style('css/other.css') !!}
    {!! Html::style('css/font.css') !!}
    {!! Html::style('css/font-awesome.css') !!}
    {!! Html::style('css/admin.css') !!}
</head>
<body>

<header class="row-layout header-page">
    <div class="col-fixed branding">
        <a href="{{ route('dashboard') }}"><strong>Wine</strong> System</a>
    </div>

    <nav class="pull-right profile-menu drop-down">
        <a href="javascript:void(0)" class="row-layout middle drop-down-toggle">
            {!! Html::image('/images/no_user.jpg', Auth::user()->full_name, ['class' => 'size-30x30 img-circle']) !!}
            <span>{{ Auth::user()->full_name }}</span>
            <i class="fa fa-angle-down"></i>
        </a>
        <ul class="drop-down-menu">
            <li><a href="{!! route('logout') !!}"><i class="fa fa-sign-out"></i> Выход</a></li>
        </ul>
    </nav>
</header>

<aside class="sidebar-left">
    <nav class="main-menu">
        <ul>@include('vendor.menu.admin', ['items' => $adminMenu->roots()])</ul>
    </nav>
</aside>

<article class="content">
    <header class="header-content">
        <h1>@yield('section')</h1>

        <nav class="bread-crumb">
            @yield('breadcrumbs')
        </nav>
    </header>

    @include('vendor.message.default')

    {{ $slot }}
</article>

{!! Html::script('js/jquery.js') !!}
{!! Html::script('js/select.js') !!}
{!! Html::script('js/notifications.js') !!}
{!! Html::script('js/admin.js') !!}
</body>
</html>
