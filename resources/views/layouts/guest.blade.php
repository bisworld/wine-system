<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Login</title>
    {!! Html::favicon('favicon.ico') !!}
    {!! Html::style('css/reset.css') !!}
    {!! Html::style('css/grid.css') !!}
    {!! Html::style('css/font.css') !!}
    {!! Html::style('css/font-awesome.css') !!}
    {!! Html::style('css/message.css') !!}
    {!! Html::style('css/login.css') !!}
</head>
<body>
    {{ $slot }}
</body>
</html>
