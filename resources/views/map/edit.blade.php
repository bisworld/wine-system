@section('title', 'Редактирование Операции')
@section('section', 'Управление Операциями')
@section('breadcrumbs', Breadcrumbs::render('map.edit', $map))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Операции</h2>
        </header>

        {!! Form::model($map, ['route' => ['map.update', $map->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('map.form', ['map' => $map])
        {!! Form::close() !!}
    </section>
</x-app-layout>
