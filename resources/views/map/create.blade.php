@section('title', 'Добавление Операции')
@section('section', 'Управление Операциями')
@section('breadcrumbs', Breadcrumbs::render('map.create'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Операции</h2>
        </header>

        {!! Form::open(['route' => 'map.store', 'class' => 'form body-tile']) !!}
        @include('map.form', ['map' => $map])
        {!! Form::close() !!}
    </section>
</x-app-layout>
