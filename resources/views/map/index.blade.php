@section('title', 'Обзор Операции')
@section('section', 'Управление Операциями')
@section('breadcrumbs', Breadcrumbs::render('map.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Операций</h2>

            <nav class="controls">
                <a href="{!! route('map.create') !!}"><i class="fa fa-plus"></i> Добавить Операцию</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Наименование</th>
                <th>Ед. Измерения</th>
                <th>Требования</th>
                <th>Объем Работ</th>
                <th>Срок Выполнения</th>
                <th>Норма Выработки</th>
                <th>Кол-во Дней</th>
                <th>Дата Создания</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($maps as $map)
                <tr>
                    <td>{{ $map->id }}.</td>
                    <td>{{ $map->name }}</td>
                    <td>{{ $map->unit }}</td>
                    <td>{{ $map->demand }}</td>
                    <td>{{ $map->volume }}</td>
                    <td>{{ $map->term }}</td>
                    <td>{{ $map->norm }}</td>
                    <td>{{ $map->days }}</td>
                    <td>@datetime($map->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('map.show', [$map->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('map.edit', [$map->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $map->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="10">
                        <div class="message warning" role="alert">Операции не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $maps->links() }}
    </section>
</x-app-layout>
