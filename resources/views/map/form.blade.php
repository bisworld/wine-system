<div class="row">
    <div class="col form-item">
        {!! Form::label('name', 'Наименование:') !!}
        {!! Form::text('name', null, ['class' => 'inp', 'placeholder' => 'Наименование', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('unit', 'Ед. Измерения:') !!}
        {!! Form::text('unit', null, ['class' => 'inp', 'placeholder' => 'Куст', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('demand', 'Требования:') !!}
        {!! Form::text('demand', null, ['class' => 'inp', 'placeholder' => 'Требования']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('volume', 'Объем Работ:') !!}
        {!! Form::text('volume', null, ['class' => 'inp', 'placeholder' => 'Объем', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('term', 'Срок Выполнения:') !!}
        {!! Form::text('term', null, ['class' => 'inp', 'placeholder' => 'Срок', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('norm', 'Норма Выработки:') !!}
        {!! Form::text('norm', null, ['class' => 'inp', 'placeholder' => 'Норма']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('days', 'Кол-во Дней:') !!}
        {!! Form::text('days', null, ['class' => 'inp', 'placeholder' => 'Дней']) !!}
    </div>
</div>

<footer class="row footer-tile">
    <a href="{{ URL::previous() }}" class="button light-red">Отмена</a>
    <input type="submit" value="Сохранить" class="button success">
</footer>
