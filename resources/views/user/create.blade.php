@section('title', 'Добавление Сотрудника')
@section('section', 'Управление Сотрудниками')
@section('breadcrumbs', Breadcrumbs::render('user.create'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Сотрудника</h2>
        </header>

        {!! Form::open(['route' => 'user.store', 'class' => 'form body-tile']) !!}
        @include('user.form', ['user' => $user])
        {!! Form::close() !!}
    </section>
</x-app-layout>
