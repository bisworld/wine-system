@section('title', 'Обзор Сотрудников')
@section('section', 'Управление Сотрудниками')
@section('breadcrumbs', Breadcrumbs::render('user.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Сотрудников</h2>

            <nav class="controls">
                <a href="{!! route('user.create') !!}"><i class="fa fa-plus"></i> Добавить Сотрудника</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Отчество</th>
                <th>Телефон</th>
                <th>Роль</th>
                <th class="center">Статус</th>
                <th>Дата Регистрации</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($users as $user)
                <tr>
                    <td>{{ $user->id }}.</td>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->middle_name }}</td>
                    <td>{{ $user->phone }}</td>
                    <td>{{ $user->role_name }}</td>
                    <td class="center">{!! $user->status_icon !!}</td>
                    <td>@datetime($user->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('user.show', [$user->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('user.edit', [$user->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $user->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9">
                        <div class="message warning" role="alert">Сотрудники не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $users->links() }}
    </section>
</x-app-layout>
