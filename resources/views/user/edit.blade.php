@section('title', 'Редактирование Сотрудника')
@section('section', 'Управление Сотрудниками')
@section('breadcrumbs', Breadcrumbs::render('user.edit', $user))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Сотрудника</h2>
        </header>

        {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('user.form', ['user' => $user])
        {!! Form::close() !!}
    </section>
</x-app-layout>
