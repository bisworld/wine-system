@section('title', 'Просмотр Сотрудника')
@section('section', 'Управление Сотрудниками')
@section('breadcrumbs', Breadcrumbs::render('user.show', $user))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Клиента</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Имя:</span>
                    <span class="inp">{{ $user->first_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Фамилия:</span>
                    <span class="inp">{{ $user->last_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Отчество:</span>
                    <span class="inp">{{ $user->middle_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Telegram:</span>
                    <span class="inp">{{ $user->telegram }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Телефон:</span>
                    <span class="inp">{{ $user->phone }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Роль:</span>
                    <span class="inp">{{ $user->role_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Статус:</span>
                    <span class="inp">{{ $user->status_text }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Дата Создания:</span>
                    <span class="inp">{{ $user->created_at }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Дата Изменения:</span>
                    <span class="inp">{{ $user->updated_at }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
</x-app-layout>
