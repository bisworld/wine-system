@section('title', 'Обзор Задач')
@section('section', 'Управление Задачами')
@section('breadcrumbs', Breadcrumbs::render('task.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Задач</h2>

            <nav class="controls">
                <a href="{!! route('task.create') !!}"><i class="fa fa-plus"></i> Добавить Задачу</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Наименование</th>
                <th>Сотрудник</th>
                <th>Комментарий</th>
                <th class="center">Статус</th>
                <th>Дата Создания</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($tasks as $task)
                <tr>
                    <td>{{ $task->id }}.</td>
                    <td>{{ $task->map->name }}</td>
                    <td>{{ $task->user->full_name }}</td>
                    <td>{{ $task->comment }}</td>
                    <td class="center">{!! $task->status_icon !!}</td>
                    <td>@datetime($task->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('task.show', [$task->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('task.edit', [$task->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $task->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">
                        <div class="message warning" role="alert">Задачи не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $tasks->links() }}
    </section>
</x-app-layout>
