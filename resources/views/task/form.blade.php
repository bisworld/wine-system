<div class="row">
    <div class="col form-item">
        {!! Form::label('map_id', 'Наименование:') !!}
        {!! Form::select('map_id', \App\Models\Map::all()->pluck('name', 'id')->toArray(), null, ['class' => 'select', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('user_id', 'Сотрудник:') !!}
        {!! Form::select('user_id', \App\Models\User::all()->pluck('full_name', 'id')->toArray(), null, ['class' => 'select', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('comment', 'Комментарий:') !!}
        {!! Form::textarea('comment', null, ['class' => 'inp', 'placeholder' => 'Комментарий', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('status', 'Статус:') !!}
        {!! Form::select('status', $task->statusList(), null, ['class' => 'select', 'required']) !!}
    </div>
    <div class="col form-item"></div>
</div>

<footer class="row footer-tile">
    <a href="{{ URL::previous() }}" class="button light-red">Отмена</a>
    <input type="submit" value="Сохранить" class="button success">
</footer>
