@section('title', 'Добавление Задачи')
@section('section', 'Управление Задачами')
@section('breadcrumbs', Breadcrumbs::render('task.create'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Задачи</h2>
        </header>

        {!! Form::open(['route' => 'task.store', 'class' => 'form body-tile']) !!}
        @include('task.form', ['task' => $task])
        {!! Form::close() !!}
    </section>
</x-app-layout>
