@section('title', 'Просмотр Задач')
@section('section', 'Управление Задачами')
@section('breadcrumbs', Breadcrumbs::render('task.show', $task))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Задачи</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Имя:</span>
                    <span class="inp">{{ $task->first_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Фамилия:</span>
                    <span class="inp">{{ $task->last_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Отчество:</span>
                    <span class="inp">{{ $task->middle_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Telegram:</span>
                    <span class="inp">{{ $task->telegram }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Телефон:</span>
                    <span class="inp">{{ $task->phone }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Роль:</span>
                    <span class="inp">{{ $task->role_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Статус:</span>
                    <span class="inp">{{ $task->status_text }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Дата Создания:</span>
                    <span class="inp">{{ $task->created_at }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Дата Изменения:</span>
                    <span class="inp">{{ $task->updated_at }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
</x-app-layout>
