@section('title', 'Редактирование Задачи')
@section('section', 'Управление Задачами')
@section('breadcrumbs', Breadcrumbs::render('task.edit', $task))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Задачи</h2>
        </header>

        {!! Form::model($task, ['route' => ['task.update', $task->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('task.form', ['task' => $task])
        {!! Form::close() !!}
    </section>
</x-app-layout>
