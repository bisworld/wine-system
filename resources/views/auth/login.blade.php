<x-guest-layout>
    <section class="page login">
        <header class="logo">
            <h3 class="text-light text-white text-center">Wine <span class="text-light-red">System</span></h3>
        </header>
        <section class="box">
            {!! Form::open(['route' => 'login', 'class' => 'form']) !!}
            <header class="header">
                <h2 class="text-light text-green-sea text-center">Log In</h2>
            </header>
            <div class="form-item">
                {!! Form::tel('phone', 79001234567, ['class' => 'inp', 'placeholder' => 'Phone', 'required']) !!}
            </div>
            <div class="form-item">
                {!! Form::text('password', 'password', ['class' => 'inp', 'placeholder' => 'Password', 'required']) !!}
            </div>
            <div class="form-item checkboxes-inline">
                {!! Form::checkbox('remember', true, NULL, ['id' => 'remember']) !!}
                {!! Form::label('remember', 'Remember me') !!}
            </div>

            <footer class="footer bg-slate-gray row between">
                {!! Form::reset('Clear', ['class' => 'button light-red upper']) !!}
                {!! Form::submit('Login', ['class' => 'button success upper']) !!}
            </footer>
            {!! Form::close() !!}
        </section>

        <div style="width: 420px; margin: 20px auto 0">
            @include('vendor.message.default')
        </div>
    </section>
</x-guest-layout>
